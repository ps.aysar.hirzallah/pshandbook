

<h2><a id="user-content-getting-started" class="anchor" aria-hidden="true" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Getting started</h2>
<h3><a id="user-content-introduction" class="anchor" aria-hidden="true" href="#introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Introduction</h3>
<p>This is a quick reference to getting started with Bash scripting.</p>
<ul>
<li><a href="https://learnxinyminutes.com/docs/bash/" rel="nofollow">Learn bash in y minutes</a> <em>(learnxinyminutes.com)</em></li>
<li><a href="http://mywiki.wooledge.org/BashGuide" rel="nofollow">Bash Guide</a> <em>(mywiki.wooledge.org)</em></li>
</ul>
<h3><a id="user-content-example" class="anchor" aria-hidden="true" href="#example"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Example</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#!</span>/usr/bin/env bash</span>

NAME=<span class="pl-s"><span class="pl-pds">"</span>John<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>Hello <span class="pl-smi">$NAME</span>!<span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-variables" class="anchor" aria-hidden="true" href="#variables"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Variables</h3>
<div class="highlight highlight-source-shell"><pre>NAME=<span class="pl-s"><span class="pl-pds">"</span>John<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-smi">$NAME</span>
<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$NAME</span><span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${NAME}</span>!<span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-string-quotes" class="anchor" aria-hidden="true" href="#string-quotes"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>String quotes</h3>
<div class="highlight highlight-source-shell"><pre>NAME=<span class="pl-s"><span class="pl-pds">"</span>John<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>Hi <span class="pl-smi">$NAME</span><span class="pl-pds">"</span></span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; Hi John</span>
<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">'</span>Hi $NAME<span class="pl-pds">'</span></span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; Hi $NAME</span></pre></div>
<h3><a id="user-content-shell-execution" class="anchor" aria-hidden="true" href="#shell-execution"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Shell execution</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>I'm in <span class="pl-s"><span class="pl-pds">$(</span>pwd<span class="pl-pds">)</span></span><span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>I'm in <span class="pl-s"><span class="pl-pds">`</span>pwd<span class="pl-pds">`</span></span><span class="pl-pds">"</span></span>
<span class="pl-c"><span class="pl-c">#</span> Same</span></pre></div>
<p>See <a href="http://wiki.bash-hackers.org/syntax/expansion/cmdsubst" rel="nofollow">Command substitution</a></p>
<h3><a id="user-content-conditional-execution" class="anchor" aria-hidden="true" href="#conditional-execution"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Conditional execution</h3>
<div class="highlight highlight-source-shell"><pre>git commit <span class="pl-k">&amp;&amp;</span> git push
git commit <span class="pl-k">||</span> <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>Commit failed<span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-functions" class="anchor" aria-hidden="true" href="#functions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Functions</h3>
<p>{: id='functions-example'}</p>
<div class="highlight highlight-source-shell"><pre><span class="pl-en">get_name</span>() {
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>John<span class="pl-pds">"</span></span>
}

<span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>You are <span class="pl-s"><span class="pl-pds">$(</span>get_name<span class="pl-pds">)</span></span><span class="pl-pds">"</span></span></pre></div>
<p>See: <a href="#functions">Functions</a></p>
<h3><a id="user-content-conditionals" class="anchor" aria-hidden="true" href="#conditionals"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Conditionals</h3>
<p>{: id='conditionals-example'}</p>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">if</span> [[ <span class="pl-k">-z</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$string</span><span class="pl-pds">"</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>String is empty<span class="pl-pds">"</span></span>
<span class="pl-k">elif</span> [[ <span class="pl-k">-n</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$string</span><span class="pl-pds">"</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>String is not empty<span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
<p>See: <a href="#conditionals">Conditionals</a></p>
<h3><a id="user-content-strict-mode" class="anchor" aria-hidden="true" href="#strict-mode"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Strict mode</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">set</span> -euo pipefail
IFS=<span class="pl-s"><span class="pl-pds">$'</span><span class="pl-cce">\n\t</span><span class="pl-pds">'</span></span></pre></div>
<p>See: <a href="http://redsymbol.net/articles/unofficial-bash-strict-mode/" rel="nofollow">Unofficial bash strict mode</a></p>
<h3><a id="user-content-brace-expansion" class="anchor" aria-hidden="true" href="#brace-expansion"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Brace expansion</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">echo</span> {A,B}.js</pre></div>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>{A,B}</code></td>
<td>Same as <code>A B</code></td>
</tr>
<tr>
<td><code>{A,B}.js</code></td>
<td>Same as <code>A.js B.js</code></td>
</tr>
<tr>
<td><code>{1..5}</code></td>
<td>Same as <code>1 2 3 4 5</code></td>
</tr>
</tbody>
</table>
<p>See: <a href="http://wiki.bash-hackers.org/syntax/expansion/brace" rel="nofollow">Brace expansion</a></p>
<h2><a id="user-content-parameter-expansions" class="anchor" aria-hidden="true" href="#parameter-expansions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Parameter expansions</h2>
<p>{: .-three-column}</p>
<h3><a id="user-content-basics" class="anchor" aria-hidden="true" href="#basics"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Basics</h3>
<div class="highlight highlight-source-shell"><pre>name=<span class="pl-s"><span class="pl-pds">"</span>John<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name}</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">/</span>J<span class="pl-k">/</span>j}</span>    <span class="pl-c"><span class="pl-c">#</span>=&gt; "john" (substitution)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">:</span>0<span class="pl-k">:</span>2}</span>    <span class="pl-c"><span class="pl-c">#</span>=&gt; "Jo" (slicing)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">::</span>2}</span>     <span class="pl-c"><span class="pl-c">#</span>=&gt; "Jo" (slicing)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">::-</span>1}</span>    <span class="pl-c"><span class="pl-c">#</span>=&gt; "Joh" (slicing)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">:</span>(-1)}</span>   <span class="pl-c"><span class="pl-c">#</span>=&gt; "n" (slicing from right)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">:</span>(-2)<span class="pl-k">:</span>1}</span> <span class="pl-c"><span class="pl-c">#</span>=&gt; "h" (slicing from right)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${food<span class="pl-k">:-</span>Cake}</span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; $food or "Cake"</span></pre></div>
<div class="highlight highlight-source-shell"><pre>length=2
<span class="pl-c1">echo</span> <span class="pl-smi">${name<span class="pl-k">:</span>0<span class="pl-k">:</span>length}</span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; "Jo"</span></pre></div>
<p>See: <a href="http://wiki.bash-hackers.org/syntax/pe" rel="nofollow">Parameter expansion</a></p>
<div class="highlight highlight-source-shell"><pre>STR=<span class="pl-s"><span class="pl-pds">"</span>/path/to/foo.cpp<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">%</span>.cpp}</span>    <span class="pl-c"><span class="pl-c">#</span> /path/to/foo</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">%</span>.cpp}</span>.o  <span class="pl-c"><span class="pl-c">#</span> /path/to/foo.o</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">%/*</span>}</span>      <span class="pl-c"><span class="pl-c">#</span> /path/to</span>

<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">##*</span>.}</span>     <span class="pl-c"><span class="pl-c">#</span> cpp (extension)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">##*/</span>}</span>     <span class="pl-c"><span class="pl-c">#</span> foo.cpp (basepath)</span>

<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">#*/</span>}</span>      <span class="pl-c"><span class="pl-c">#</span> path/to/foo.cpp</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">##*/</span>}</span>     <span class="pl-c"><span class="pl-c">#</span> foo.cpp</span>

<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">/</span>foo<span class="pl-k">/</span>bar}</span> <span class="pl-c"><span class="pl-c">#</span> /path/to/bar.cpp</span></pre></div>
<div class="highlight highlight-source-shell"><pre>STR=<span class="pl-s"><span class="pl-pds">"</span>Hello world<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">:</span>6<span class="pl-k">:</span>5}</span>   <span class="pl-c"><span class="pl-c">#</span> "world"</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR<span class="pl-k">:</span> -5<span class="pl-k">:</span>5}</span>  <span class="pl-c"><span class="pl-c">#</span> "world"</span></pre></div>
<div class="highlight highlight-source-shell"><pre>SRC=<span class="pl-s"><span class="pl-pds">"</span>/path/to/foo.cpp<span class="pl-pds">"</span></span>
BASE=<span class="pl-smi">${SRC<span class="pl-k">##*/</span>}</span>   <span class="pl-c"><span class="pl-c">#</span>=&gt; "foo.cpp" (basepath)</span>
DIR=<span class="pl-smi">${SRC<span class="pl-k">%</span><span class="pl-smi">$BASE</span>}</span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; "/path/to/" (dirpath)</span></pre></div>
<h3><a id="user-content-substitution" class="anchor" aria-hidden="true" href="#substitution"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Substitution</h3>
<table>
<thead>
<tr>
<th>Code</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>${FOO%suffix}</code></td>
<td>Remove suffix</td>
</tr>
<tr>
<td><code>${FOO#prefix}</code></td>
<td>Remove prefix</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>${FOO%%suffix}</code></td>
<td>Remove long suffix</td>
</tr>
<tr>
<td><code>${FOO##prefix}</code></td>
<td>Remove long prefix</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>${FOO/from/to}</code></td>
<td>Replace first match</td>
</tr>
<tr>
<td><code>${FOO//from/to}</code></td>
<td>Replace all</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>${FOO/%from/to}</code></td>
<td>Replace suffix</td>
</tr>
<tr>
<td><code>${FOO/#from/to}</code></td>
<td>Replace prefix</td>
</tr>
</tbody>
</table>
<h3><a id="user-content-comments" class="anchor" aria-hidden="true" href="#comments"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Comments</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#</span> Single line comment</span></pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">:</span> <span class="pl-s"><span class="pl-pds">'</span></span>
<span class="pl-s">This is a</span>
<span class="pl-s">multi line</span>
<span class="pl-s">comment</span>
<span class="pl-s"><span class="pl-pds">'</span></span></pre></div>
<h3><a id="user-content-substrings" class="anchor" aria-hidden="true" href="#substrings"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Substrings</h3>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>${FOO:0:3}</code></td>
<td>Substring <em>(position, length)</em></td>
</tr>
<tr>
<td><code>${FOO:(-3):3}</code></td>
<td>Substring from the right</td>
</tr>
</tbody>
</table>
<h3><a id="user-content-length" class="anchor" aria-hidden="true" href="#length"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Length</h3>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>${#FOO}</code></td>
<td>Length of <code>$FOO</code></td>
</tr>
</tbody>
</table>
<h3><a id="user-content-manipulation" class="anchor" aria-hidden="true" href="#manipulation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Manipulation</h3>
<div class="highlight highlight-source-shell"><pre>STR=<span class="pl-s"><span class="pl-pds">"</span>HELLO WORLD!<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR,}</span>   <span class="pl-c"><span class="pl-c">#</span>=&gt; "hELLO WORLD!" (lowercase 1st letter)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR,,}</span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; "hello world!" (all lowercase)</span>

STR=<span class="pl-s"><span class="pl-pds">"</span>hello world!<span class="pl-pds">"</span></span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR^}</span>   <span class="pl-c"><span class="pl-c">#</span>=&gt; "Hello world!" (uppercase 1st letter)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${STR^^}</span>  <span class="pl-c"><span class="pl-c">#</span>=&gt; "HELLO WORLD!" (all uppercase)</span></pre></div>
<h3><a id="user-content-default-values" class="anchor" aria-hidden="true" href="#default-values"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Default values</h3>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>${FOO:-val}</code></td>
<td><code>$FOO</code>, or <code>val</code> if unset (or null)</td>
</tr>
<tr>
<td><code>${FOO:=val}</code></td>
<td>Set <code>$FOO</code> to <code>val</code> if unset (or null)</td>
</tr>
<tr>
<td><code>${FOO:+val}</code></td>
<td><code>val</code> if <code>$FOO</code> is set (and not null)</td>
</tr>
<tr>
<td><code>${FOO:?message}</code></td>
<td>Show error message and exit if <code>$FOO</code> is unset (or null)</td>
</tr>
</tbody>
</table>
<p>Omitting the <code>:</code> removes the (non)nullity checks, e.g. <code>${FOO-val}</code> expands to <code>val</code> if unset otherwise <code>$FOO</code>.</p>
<h2><a id="user-content-loops" class="anchor" aria-hidden="true" href="#loops"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Loops</h2>
<p>{: .-three-column}</p>
<h3><a id="user-content-basic-for-loop" class="anchor" aria-hidden="true" href="#basic-for-loop"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Basic for loop</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-smi">i</span> <span class="pl-k">in</span> /etc/rc.<span class="pl-k">*</span><span class="pl-k">;</span> <span class="pl-k">do</span>
  <span class="pl-c1">echo</span> <span class="pl-smi">$i</span>
<span class="pl-k">done</span></pre></div>
<h3><a id="user-content-c-like-for-loop" class="anchor" aria-hidden="true" href="#c-like-for-loop"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>C-like for loop</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-s"><span class="pl-pds">((</span>i <span class="pl-k">=</span> <span class="pl-c1">0</span> ; i <span class="pl-k">&lt;</span> <span class="pl-c1">100</span> ; i<span class="pl-k">++</span><span class="pl-pds">))</span></span><span class="pl-k">;</span> <span class="pl-k">do</span>
  <span class="pl-c1">echo</span> <span class="pl-smi">$i</span>
<span class="pl-k">done</span></pre></div>
<h3><a id="user-content-ranges" class="anchor" aria-hidden="true" href="#ranges"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Ranges</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-smi">i</span> <span class="pl-k">in</span> {1..5}<span class="pl-k">;</span> <span class="pl-k">do</span>
    <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>Welcome <span class="pl-smi">$i</span><span class="pl-pds">"</span></span>
<span class="pl-k">done</span></pre></div>
<h4><a id="user-content-with-step-size" class="anchor" aria-hidden="true" href="#with-step-size"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>With step size</h4>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-smi">i</span> <span class="pl-k">in</span> {5..50..5}<span class="pl-k">;</span> <span class="pl-k">do</span>
    <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>Welcome <span class="pl-smi">$i</span><span class="pl-pds">"</span></span>
<span class="pl-k">done</span></pre></div>
<h3><a id="user-content-reading-lines" class="anchor" aria-hidden="true" href="#reading-lines"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Reading lines</h3>
<div class="highlight highlight-source-shell"><pre>cat file.txt <span class="pl-k">|</span> <span class="pl-k">while</span> <span class="pl-c1">read</span> line<span class="pl-k">;</span> <span class="pl-k">do</span>
  <span class="pl-c1">echo</span> <span class="pl-smi">$line</span>
<span class="pl-k">done</span></pre></div>
<h3><a id="user-content-forever" class="anchor" aria-hidden="true" href="#forever"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Forever</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">while</span> <span class="pl-c1">true</span><span class="pl-k">;</span> <span class="pl-k">do</span>
  ···
<span class="pl-k">done</span></pre></div>
<h2><a id="user-content-functions-1" class="anchor" aria-hidden="true" href="#functions-1"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Functions</h2>
<p>{: .-three-column}</p>
<h3><a id="user-content-defining-functions" class="anchor" aria-hidden="true" href="#defining-functions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Defining functions</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-en">myfunc</span>() {
    <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>hello <span class="pl-smi">$1</span><span class="pl-pds">"</span></span>
}</pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#</span> Same as above (alternate syntax)</span>
<span class="pl-k">function</span> <span class="pl-en">myfunc()</span> {
    <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>hello <span class="pl-smi">$1</span><span class="pl-pds">"</span></span>
}</pre></div>
<div class="highlight highlight-source-shell"><pre>myfunc <span class="pl-s"><span class="pl-pds">"</span>John<span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-returning-values" class="anchor" aria-hidden="true" href="#returning-values"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Returning values</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-en">myfunc</span>() {
    <span class="pl-k">local</span> myresult=<span class="pl-s"><span class="pl-pds">'</span>some value<span class="pl-pds">'</span></span>
    <span class="pl-c1">echo</span> <span class="pl-smi">$myresult</span>
}</pre></div>
<div class="highlight highlight-source-shell"><pre>result=<span class="pl-s"><span class="pl-pds">"</span><span class="pl-s"><span class="pl-pds">$(</span>myfunc<span class="pl-pds">)</span></span><span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-raising-errors" class="anchor" aria-hidden="true" href="#raising-errors"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Raising errors</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-en">myfunc</span>() {
  <span class="pl-k">return</span> 1
}</pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">if</span> myfunc<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>success<span class="pl-pds">"</span></span>
<span class="pl-k">else</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>failure<span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
<h3><a id="user-content-arguments" class="anchor" aria-hidden="true" href="#arguments"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Arguments</h3>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>$#</code></td>
<td>Number of arguments</td>
</tr>
<tr>
<td><code>$*</code></td>
<td>All arguments</td>
</tr>
<tr>
<td><code>$@</code></td>
<td>All arguments, starting from first</td>
</tr>
<tr>
<td><code>$1</code></td>
<td>First argument</td>
</tr>
<tr>
<td><code>$_</code></td>
<td>Last argument of the previous command</td>
</tr>
</tbody>
</table>
<p>See <a href="http://wiki.bash-hackers.org/syntax/shellvars#special_parameters_and_shell_variables" rel="nofollow">Special parameters</a>.</p>
<h2><a id="user-content-conditionals-1" class="anchor" aria-hidden="true" href="#conditionals-1"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Conditionals</h2>
<p>{: .-three-column}</p>
<h3><a id="user-content-conditions" class="anchor" aria-hidden="true" href="#conditions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Conditions</h3>
<p>Note that <code>[[</code> is actually a command/program that returns either <code>0</code> (true) or <code>1</code> (false). Any program that obeys the same logic (like all base utils, such as <code>grep(1)</code> or <code>ping(1)</code>) can be used as condition, see examples.</p>
<table>
<thead>
<tr>
<th>Condition</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>[[ -z STRING ]]</code></td>
<td>Empty string</td>
</tr>
<tr>
<td><code>[[ -n STRING ]]</code></td>
<td>Not empty string</td>
</tr>
<tr>
<td><code>[[ STRING == STRING ]]</code></td>
<td>Equal</td>
</tr>
<tr>
<td><code>[[ STRING != STRING ]]</code></td>
<td>Not Equal</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>[[ NUM -eq NUM ]]</code></td>
<td>Equal</td>
</tr>
<tr>
<td><code>[[ NUM -ne NUM ]]</code></td>
<td>Not equal</td>
</tr>
<tr>
<td><code>[[ NUM -lt NUM ]]</code></td>
<td>Less than</td>
</tr>
<tr>
<td><code>[[ NUM -le NUM ]]</code></td>
<td>Less than or equal</td>
</tr>
<tr>
<td><code>[[ NUM -gt NUM ]]</code></td>
<td>Greater than</td>
</tr>
<tr>
<td><code>[[ NUM -ge NUM ]]</code></td>
<td>Greater than or equal</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>[[ STRING =~ STRING ]]</code></td>
<td>Regexp</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>(( NUM &lt; NUM ))</code></td>
<td>Numeric conditions</td>
</tr>
</tbody>
</table>
<h4><a id="user-content-more-conditions" class="anchor" aria-hidden="true" href="#more-conditions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>More conditions</h4>
<table>
<thead>
<tr>
<th>Condition</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>[[ -o noclobber ]]</code></td>
<td>If OPTIONNAME is enabled</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>[[ ! EXPR ]]</code></td>
<td>Not</td>
</tr>
<tr>
<td><code>[[ X &amp;&amp; Y ]]</code></td>
<td>And</td>
</tr>
<tr>
<td>`[[ X</td>
<td></td>
</tr>
</tbody>
</table>
<h3><a id="user-content-file-conditions" class="anchor" aria-hidden="true" href="#file-conditions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>File conditions</h3>
<table>
<thead>
<tr>
<th>Condition</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>[[ -e FILE ]]</code></td>
<td>Exists</td>
</tr>
<tr>
<td><code>[[ -r FILE ]]</code></td>
<td>Readable</td>
</tr>
<tr>
<td><code>[[ -h FILE ]]</code></td>
<td>Symlink</td>
</tr>
<tr>
<td><code>[[ -d FILE ]]</code></td>
<td>Directory</td>
</tr>
<tr>
<td><code>[[ -w FILE ]]</code></td>
<td>Writable</td>
</tr>
<tr>
<td><code>[[ -s FILE ]]</code></td>
<td>Size is &gt; 0 bytes</td>
</tr>
<tr>
<td><code>[[ -f FILE ]]</code></td>
<td>File</td>
</tr>
<tr>
<td><code>[[ -x FILE ]]</code></td>
<td>Executable</td>
</tr>
<tr>
<td>---</td>
<td>---</td>
</tr>
<tr>
<td><code>[[ FILE1 -nt FILE2 ]]</code></td>
<td>1 is more recent than 2</td>
</tr>
<tr>
<td><code>[[ FILE1 -ot FILE2 ]]</code></td>
<td>2 is more recent than 1</td>
</tr>
<tr>
<td><code>[[ FILE1 -ef FILE2 ]]</code></td>
<td>Same files</td>
</tr>
</tbody>
</table>
<h3><a id="user-content-example-1" class="anchor" aria-hidden="true" href="#example-1"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Example</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#</span> String</span>
<span class="pl-k">if</span> [[ <span class="pl-k">-z</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$string</span><span class="pl-pds">"</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>String is empty<span class="pl-pds">"</span></span>
<span class="pl-k">elif</span> [[ <span class="pl-k">-n</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$string</span><span class="pl-pds">"</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>String is not empty<span class="pl-pds">"</span></span>
<span class="pl-k">else</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>This never happens<span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#</span> Combinations</span>
<span class="pl-k">if</span> [[ X <span class="pl-k">&amp;&amp;</span> Y ]]<span class="pl-k">;</span> <span class="pl-k">then</span>
  ...
<span class="pl-k">fi</span></pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#</span> Equal</span>
<span class="pl-k">if</span> [[ <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$A</span><span class="pl-pds">"</span></span> <span class="pl-k">==</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$B</span><span class="pl-pds">"</span></span> ]]</pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-c"><span class="pl-c">#</span> Regex</span>
<span class="pl-k">if</span> [[ <span class="pl-s"><span class="pl-pds">"</span>A<span class="pl-pds">"</span></span> <span class="pl-k">=~</span> <span class="pl-c1">.</span> ]]</pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">if</span> <span class="pl-s"><span class="pl-pds">((</span> <span class="pl-smi">$a</span> <span class="pl-k">&lt;</span> <span class="pl-smi">$b</span> <span class="pl-pds">))</span></span><span class="pl-k">;</span> <span class="pl-k">then</span>
   <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$a</span> is smaller than <span class="pl-smi">$b</span><span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">if</span> [[ <span class="pl-k">-e</span> <span class="pl-s"><span class="pl-pds">"</span>file.txt<span class="pl-pds">"</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>file exists<span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
<h2><a id="user-content-arrays" class="anchor" aria-hidden="true" href="#arrays"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Arrays</h2>
<h3><a id="user-content-defining-arrays" class="anchor" aria-hidden="true" href="#defining-arrays"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Defining arrays</h3>
<div class="highlight highlight-source-shell"><pre>Fruits=(<span class="pl-s"><span class="pl-pds">'</span>Apple<span class="pl-pds">'</span></span> <span class="pl-s"><span class="pl-pds">'</span>Banana<span class="pl-pds">'</span></span> <span class="pl-s"><span class="pl-pds">'</span>Orange<span class="pl-pds">'</span></span>)</pre></div>
<div class="highlight highlight-source-shell"><pre>Fruits[0]=<span class="pl-s"><span class="pl-pds">"</span>Apple<span class="pl-pds">"</span></span>
Fruits[1]=<span class="pl-s"><span class="pl-pds">"</span>Banana<span class="pl-pds">"</span></span>
Fruits[2]=<span class="pl-s"><span class="pl-pds">"</span>Orange<span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-working-with-arrays" class="anchor" aria-hidden="true" href="#working-with-arrays"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Working with arrays</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">echo</span> <span class="pl-smi">${Fruits[0]}</span>           <span class="pl-c"><span class="pl-c">#</span> Element #0</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${Fruits[-1]}</span>          <span class="pl-c"><span class="pl-c">#</span> Last element</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${Fruits[@]}</span>           <span class="pl-c"><span class="pl-c">#</span> All elements, space-separated</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${<span class="pl-k">#</span>Fruits[@]}</span>          <span class="pl-c"><span class="pl-c">#</span> Number of elements</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${<span class="pl-k">#</span>Fruits}</span>             <span class="pl-c"><span class="pl-c">#</span> String length of the 1st element</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${<span class="pl-k">#</span>Fruits[3]}</span>          <span class="pl-c"><span class="pl-c">#</span> String length of the Nth element</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${Fruits[@]<span class="pl-k">:</span>3<span class="pl-k">:</span>2}</span>       <span class="pl-c"><span class="pl-c">#</span> Range (from position 3, length 2)</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${<span class="pl-k">!</span>Fruits[@]}</span>          <span class="pl-c"><span class="pl-c">#</span> Keys of all elements, space-separated</span></pre></div>
<h3><a id="user-content-operations" class="anchor" aria-hidden="true" href="#operations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Operations</h3>
<div class="highlight highlight-source-shell"><pre>Fruits=(<span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${Fruits[@]}</span><span class="pl-pds">"</span></span> <span class="pl-s"><span class="pl-pds">"</span>Watermelon<span class="pl-pds">"</span></span>)    <span class="pl-c"><span class="pl-c">#</span> Push</span>
Fruits+=(<span class="pl-s"><span class="pl-pds">'</span>Watermelon<span class="pl-pds">'</span></span>)                  <span class="pl-c"><span class="pl-c">#</span> Also Push</span>
Fruits=( <span class="pl-smi">${Fruits[@]<span class="pl-k">/</span>Ap<span class="pl-k">*/</span>}</span> )            <span class="pl-c"><span class="pl-c">#</span> Remove by regex match</span>
<span class="pl-c1">unset</span> Fruits[2]                         <span class="pl-c"><span class="pl-c">#</span> Remove one item</span>
Fruits=(<span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${Fruits[@]}</span><span class="pl-pds">"</span></span>)                 <span class="pl-c"><span class="pl-c">#</span> Duplicate</span>
Fruits=(<span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${Fruits[@]}</span><span class="pl-pds">"</span></span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${Veggies[@]}</span><span class="pl-pds">"</span></span>) <span class="pl-c"><span class="pl-c">#</span> Concatenate</span>
lines=(<span class="pl-s"><span class="pl-pds">`</span>cat <span class="pl-s"><span class="pl-pds">"</span>logfile<span class="pl-pds">"</span></span><span class="pl-pds">`</span></span>)                 <span class="pl-c"><span class="pl-c">#</span> Read from file</span></pre></div>
<h3><a id="user-content-iteration" class="anchor" aria-hidden="true" href="#iteration"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Iteration</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-smi">i</span> <span class="pl-k">in</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${arrayName[@]}</span><span class="pl-pds">"</span></span><span class="pl-k">;</span> <span class="pl-k">do</span>
  <span class="pl-c1">echo</span> <span class="pl-smi">$i</span>
<span class="pl-k">done</span></pre></div>
<h2><a id="user-content-dictionaries" class="anchor" aria-hidden="true" href="#dictionaries"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Dictionaries</h2>
<p>{: .-three-column}</p>
<h3><a id="user-content-defining" class="anchor" aria-hidden="true" href="#defining"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Defining</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">declare</span> -A sounds</pre></div>
<div class="highlight highlight-source-shell"><pre>sounds[dog]=<span class="pl-s"><span class="pl-pds">"</span>bark<span class="pl-pds">"</span></span>
sounds[cow]=<span class="pl-s"><span class="pl-pds">"</span>moo<span class="pl-pds">"</span></span>
sounds[bird]=<span class="pl-s"><span class="pl-pds">"</span>tweet<span class="pl-pds">"</span></span>
sounds[wolf]=<span class="pl-s"><span class="pl-pds">"</span>howl<span class="pl-pds">"</span></span></pre></div>
<p>Declares <code>sound</code> as a Dictionary object (aka associative array).</p>
<h3><a id="user-content-working-with-dictionaries" class="anchor" aria-hidden="true" href="#working-with-dictionaries"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Working with dictionaries</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">echo</span> <span class="pl-smi">${sounds[dog]}</span> <span class="pl-c"><span class="pl-c">#</span> Dog's sound</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${sounds[@]}</span>   <span class="pl-c"><span class="pl-c">#</span> All values</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${<span class="pl-k">!</span>sounds[@]}</span>  <span class="pl-c"><span class="pl-c">#</span> All keys</span>
<span class="pl-c1">echo</span> <span class="pl-smi">${<span class="pl-k">#</span>sounds[@]}</span>  <span class="pl-c"><span class="pl-c">#</span> Number of elements</span>
<span class="pl-c1">unset</span> sounds[dog]   <span class="pl-c"><span class="pl-c">#</span> Delete dog</span></pre></div>
<h3><a id="user-content-iteration-1" class="anchor" aria-hidden="true" href="#iteration-1"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Iteration</h3>
<h4><a id="user-content-iterate-over-values" class="anchor" aria-hidden="true" href="#iterate-over-values"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Iterate over values</h4>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-smi">val</span> <span class="pl-k">in</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${sounds[@]}</span><span class="pl-pds">"</span></span><span class="pl-k">;</span> <span class="pl-k">do</span>
  <span class="pl-c1">echo</span> <span class="pl-smi">$val</span>
<span class="pl-k">done</span></pre></div>
<h4><a id="user-content-iterate-over-keys" class="anchor" aria-hidden="true" href="#iterate-over-keys"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Iterate over keys</h4>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">for</span> <span class="pl-smi">key</span> <span class="pl-k">in</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${<span class="pl-k">!</span>sounds[@]}</span><span class="pl-pds">"</span></span><span class="pl-k">;</span> <span class="pl-k">do</span>
  <span class="pl-c1">echo</span> <span class="pl-smi">$key</span>
<span class="pl-k">done</span></pre></div>
<h2><a id="user-content-options" class="anchor" aria-hidden="true" href="#options"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Options</h2>
<h3><a id="user-content-options-1" class="anchor" aria-hidden="true" href="#options-1"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Options</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">set</span> -o noclobber  <span class="pl-c"><span class="pl-c">#</span> Avoid overlay files (echo "hi" &gt; foo)</span>
<span class="pl-c1">set</span> -o errexit    <span class="pl-c"><span class="pl-c">#</span> Used to exit upon error, avoiding cascading errors</span>
<span class="pl-c1">set</span> -o pipefail   <span class="pl-c"><span class="pl-c">#</span> Unveils hidden failures</span>
<span class="pl-c1">set</span> -o nounset    <span class="pl-c"><span class="pl-c">#</span> Exposes unset variables</span></pre></div>
<h3><a id="user-content-glob-options" class="anchor" aria-hidden="true" href="#glob-options"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Glob options</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">shopt</span> -s nullglob    <span class="pl-c"><span class="pl-c">#</span> Non-matching globs are removed  ('*.foo' =&gt; '')</span>
<span class="pl-c1">shopt</span> -s failglob    <span class="pl-c"><span class="pl-c">#</span> Non-matching globs throw errors</span>
<span class="pl-c1">shopt</span> -s nocaseglob  <span class="pl-c"><span class="pl-c">#</span> Case insensitive globs</span>
<span class="pl-c1">shopt</span> -s dotglob     <span class="pl-c"><span class="pl-c">#</span> Wildcards match dotfiles ("*.sh" =&gt; ".foo.sh")</span>
<span class="pl-c1">shopt</span> -s globstar    <span class="pl-c"><span class="pl-c">#</span> Allow ** for recursive matches ('lib/**/*.rb' =&gt; 'lib/a/b/c.rb')</span></pre></div>
<p>Set <code>GLOBIGNORE</code> as a colon-separated list of patterns to be removed from glob
matches.</p>
<h2><a id="user-content-history" class="anchor" aria-hidden="true" href="#history"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>History</h2>
<h3><a id="user-content-commands" class="anchor" aria-hidden="true" href="#commands"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Commands</h3>
<table>
<thead>
<tr>
<th>Command</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>history</code></td>
<td>Show history</td>
</tr>
<tr>
<td><code>shopt -s histverify</code></td>
<td>Don't execute expanded result immediately</td>
</tr>
</tbody>
</table>
<h3><a id="user-content-expansions" class="anchor" aria-hidden="true" href="#expansions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Expansions</h3>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>!$</code></td>
<td>Expand last parameter of most recent command</td>
</tr>
<tr>
<td><code>!*</code></td>
<td>Expand all parameters of most recent command</td>
</tr>
<tr>
<td><code>!-n</code></td>
<td>Expand <code>n</code>th most recent command</td>
</tr>
<tr>
<td><code>!n</code></td>
<td>Expand <code>n</code>th command in history</td>
</tr>
<tr>
<td><code>!&lt;command&gt;</code></td>
<td>Expand most recent invocation of command <code>&lt;command&gt;</code></td>
</tr>
</tbody>
</table>
<h3><a id="user-content-operations-1" class="anchor" aria-hidden="true" href="#operations-1"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Operations</h3>
<table>
<thead>
<tr>
<th>Code</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>!!</code></td>
<td>Execute last command again</td>
</tr>
<tr>
<td><code>!!:s/&lt;FROM&gt;/&lt;TO&gt;/</code></td>
<td>Replace first occurrence of <code>&lt;FROM&gt;</code> to <code>&lt;TO&gt;</code> in most recent command</td>
</tr>
<tr>
<td><code>!!:gs/&lt;FROM&gt;/&lt;TO&gt;/</code></td>
<td>Replace all occurrences of <code>&lt;FROM&gt;</code> to <code>&lt;TO&gt;</code> in most recent command</td>
</tr>
<tr>
<td><code>!$:t</code></td>
<td>Expand only basename from last parameter of most recent command</td>
</tr>
<tr>
<td><code>!$:h</code></td>
<td>Expand only directory from last parameter of most recent command</td>
</tr>
</tbody>
</table>
<p><code>!!</code> and <code>!$</code> can be replaced with any valid expansion.</p>
<h3><a id="user-content-slices" class="anchor" aria-hidden="true" href="#slices"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Slices</h3>
<table>
<thead>
<tr>
<th>Code</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>!!:n</code></td>
<td>Expand only <code>n</code>th token from most recent command (command is <code>0</code>; first argument is <code>1</code>)</td>
</tr>
<tr>
<td><code>!^</code></td>
<td>Expand first argument from most recent command</td>
</tr>
<tr>
<td><code>!$</code></td>
<td>Expand last token from most recent command</td>
</tr>
<tr>
<td><code>!!:n-m</code></td>
<td>Expand range of tokens from most recent command</td>
</tr>
<tr>
<td><code>!!:n-$</code></td>
<td>Expand <code>n</code>th token to last from most recent command</td>
</tr>
</tbody>
</table>
<p><code>!!</code> can be replaced with any valid expansion i.e. <code>!cat</code>, <code>!-2</code>, <code>!42</code>, etc.</p>
<h2><a id="user-content-miscellaneous" class="anchor" aria-hidden="true" href="#miscellaneous"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Miscellaneous</h2>
<h3><a id="user-content-numeric-calculations" class="anchor" aria-hidden="true" href="#numeric-calculations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Numeric calculations</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-s"><span class="pl-pds">$((</span>a <span class="pl-k">+</span> <span class="pl-c1">200</span><span class="pl-pds">))</span></span>      <span class="pl-c"><span class="pl-c">#</span> Add 200 to $a</span></pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-s"><span class="pl-pds">$((</span><span class="pl-smi">$RANDOM</span><span class="pl-k">%</span><span class="pl-c1">200</span><span class="pl-pds">))</span></span>  <span class="pl-c"><span class="pl-c">#</span> Random number 0..199</span></pre></div>
<h3><a id="user-content-subshells" class="anchor" aria-hidden="true" href="#subshells"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Subshells</h3>
<div class="highlight highlight-source-shell"><pre>(cd somedir<span class="pl-k">;</span> <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>I'm now in <span class="pl-smi">$PWD</span><span class="pl-pds">"</span></span>)
<span class="pl-c1">pwd</span> <span class="pl-c"><span class="pl-c">#</span> still in first directory</span></pre></div>
<h3><a id="user-content-redirection" class="anchor" aria-hidden="true" href="#redirection"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Redirection</h3>
<div class="highlight highlight-source-shell"><pre>python hello.py <span class="pl-k">&gt;</span> output.txt   <span class="pl-c"><span class="pl-c">#</span> stdout to (file)</span>
python hello.py <span class="pl-k">&gt;&gt;</span> output.txt  <span class="pl-c"><span class="pl-c">#</span> stdout to (file), append</span>
python hello.py <span class="pl-k">2&gt;</span> error.log   <span class="pl-c"><span class="pl-c">#</span> stderr to (file)</span>
python hello.py <span class="pl-k">2&gt;&amp;1</span>           <span class="pl-c"><span class="pl-c">#</span> stderr to stdout</span>
python hello.py <span class="pl-k">2&gt;</span>/dev/null    <span class="pl-c"><span class="pl-c">#</span> stderr to (null)</span>
python hello.py <span class="pl-k">&amp;</span><span class="pl-k">&gt;</span>/dev/null    <span class="pl-c"><span class="pl-c">#</span> stdout and stderr to (null)</span></pre></div>
<div class="highlight highlight-source-shell"><pre>python hello.py <span class="pl-k">&lt;</span> foo.txt      <span class="pl-c"><span class="pl-c">#</span> feed foo.txt to stdin for python</span></pre></div>
<h3><a id="user-content-inspecting-commands" class="anchor" aria-hidden="true" href="#inspecting-commands"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Inspecting commands</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">command</span> -V <span class="pl-c1">cd</span>
<span class="pl-c"><span class="pl-c">#</span>=&gt; "cd is a function/alias/whatever"</span></pre></div>
<h3><a id="user-content-trap-errors" class="anchor" aria-hidden="true" href="#trap-errors"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Trap errors</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">trap</span> <span class="pl-s"><span class="pl-pds">'</span>echo Error at about $LINENO<span class="pl-pds">'</span></span> ERR</pre></div>
<p>or</p>
<div class="highlight highlight-source-shell"><pre><span class="pl-en">traperr</span>() {
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>ERROR: <span class="pl-smi">${BASH_SOURCE[1]}</span> at about <span class="pl-smi">${BASH_LINENO[0]}</span><span class="pl-pds">"</span></span>
}

<span class="pl-c1">set</span> -o errtrace
<span class="pl-c1">trap</span> traperr ERR</pre></div>
<h3><a id="user-content-caseswitch" class="anchor" aria-hidden="true" href="#caseswitch"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Case/switch</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">case</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$1</span><span class="pl-pds">"</span></span> <span class="pl-k">in</span>
  start | up)
    vagrant up
    ;;

  <span class="pl-k">*</span>)
    <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>Usage: <span class="pl-smi">$0</span> {start|stop|ssh}<span class="pl-pds">"</span></span>
    ;;
<span class="pl-k">esac</span></pre></div>
<h3><a id="user-content-source-relative" class="anchor" aria-hidden="true" href="#source-relative"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Source relative</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">source</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${0<span class="pl-k">%/*</span>}</span>/../share/foo.sh<span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-printf" class="anchor" aria-hidden="true" href="#printf"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>printf</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">printf</span> <span class="pl-s"><span class="pl-pds">"</span>Hello %s, I'm %s<span class="pl-pds">"</span></span> Sven Olga
<span class="pl-c"><span class="pl-c">#</span>=&gt; "Hello Sven, I'm Olga</span>

<span class="pl-c1">printf</span> <span class="pl-s"><span class="pl-pds">"</span>1 + 1 = %d<span class="pl-pds">"</span></span> 2
<span class="pl-c"><span class="pl-c">#</span>=&gt; "1 + 1 = 2"</span>

<span class="pl-c1">printf</span> <span class="pl-s"><span class="pl-pds">"</span>This is how you print a float: %f<span class="pl-pds">"</span></span> 2
<span class="pl-c"><span class="pl-c">#</span>=&gt; "This is how you print a float: 2.000000"</span></pre></div>
<h3><a id="user-content-directory-of-script" class="anchor" aria-hidden="true" href="#directory-of-script"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Directory of script</h3>
<div class="highlight highlight-source-shell"><pre>DIR=<span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">${0<span class="pl-k">%/*</span>}</span><span class="pl-pds">"</span></span></pre></div>
<h3><a id="user-content-getting-options" class="anchor" aria-hidden="true" href="#getting-options"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Getting options</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">while</span> [[ <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$1</span><span class="pl-pds">"</span></span> <span class="pl-k">=~</span> ^- <span class="pl-k">&amp;&amp;</span> <span class="pl-k">!</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$1</span><span class="pl-pds">"</span></span> <span class="pl-k">==</span> <span class="pl-s"><span class="pl-pds">"</span>--<span class="pl-pds">"</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">do</span> <span class="pl-k">case</span> <span class="pl-smi">$1</span> <span class="pl-k">in</span>
  -V | --version )
    <span class="pl-c1">echo</span> <span class="pl-smi">$version</span>
    <span class="pl-c1">exit</span>
    ;;
  -s | --string )
    <span class="pl-c1">shift</span><span class="pl-k">;</span> string=<span class="pl-smi">$1</span>
    ;;
  -f | --flag )
    flag=1
    ;;
<span class="pl-k">esac</span><span class="pl-k">;</span> <span class="pl-c1">shift</span><span class="pl-k">;</span> <span class="pl-k">done</span>
<span class="pl-k">if</span> [[ <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$1</span><span class="pl-pds">"</span></span> <span class="pl-k">==</span> <span class="pl-s"><span class="pl-pds">'</span>--<span class="pl-pds">'</span></span> ]]<span class="pl-k">;</span> <span class="pl-k">then</span> <span class="pl-c1">shift</span><span class="pl-k">;</span> <span class="pl-k">fi</span></pre></div>
<h3><a id="user-content-heredoc" class="anchor" aria-hidden="true" href="#heredoc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Heredoc</h3>
<div class="highlight highlight-source-shell"><pre>cat <span class="pl-s"><span class="pl-k">&lt;&lt;</span><span class="pl-k">END</span></span>
<span class="pl-s">hello world</span>
<span class="pl-s"><span class="pl-k">END</span></span></pre></div>
<h3><a id="user-content-reading-input" class="anchor" aria-hidden="true" href="#reading-input"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Reading input</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">echo</span> -n <span class="pl-s"><span class="pl-pds">"</span>Proceed? [y/n]: <span class="pl-pds">"</span></span>
<span class="pl-c1">read</span> ans
<span class="pl-c1">echo</span> <span class="pl-smi">$ans</span></pre></div>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">read</span> -n 1 ans    <span class="pl-c"><span class="pl-c">#</span> Just one character</span></pre></div>
<h3><a id="user-content-special-variables" class="anchor" aria-hidden="true" href="#special-variables"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Special variables</h3>
<table>
<thead>
<tr>
<th>Expression</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>$?</code></td>
<td>Exit status of last task</td>
</tr>
<tr>
<td><code>$!</code></td>
<td>PID of last background task</td>
</tr>
<tr>
<td><code>$$</code></td>
<td>PID of shell</td>
</tr>
<tr>
<td><code>$0</code></td>
<td>Filename of the shell script</td>
</tr>
</tbody>
</table>
<p>See <a href="http://wiki.bash-hackers.org/syntax/shellvars#special_parameters_and_shell_variables" rel="nofollow">Special parameters</a>.</p>
<h3><a id="user-content-go-to-previous-directory" class="anchor" aria-hidden="true" href="#go-to-previous-directory"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Go to previous directory</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-c1">pwd</span> <span class="pl-c"><span class="pl-c">#</span> /home/user/foo</span>
<span class="pl-c1">cd</span> bar/
<span class="pl-c1">pwd</span> <span class="pl-c"><span class="pl-c">#</span> /home/user/foo/bar</span>
<span class="pl-c1">cd</span> -
<span class="pl-c1">pwd</span> <span class="pl-c"><span class="pl-c">#</span> /home/user/foo</span></pre></div>
<h3><a id="user-content-check-for-commands-result" class="anchor" aria-hidden="true" href="#check-for-commands-result"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Check for command's result</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">if</span> ping -c 1 google.com<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>It appears you have a working internet connection<span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
<h3><a id="user-content-grep-check" class="anchor" aria-hidden="true" href="#grep-check"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Grep check</h3>
<div class="highlight highlight-source-shell"><pre><span class="pl-k">if</span> grep -q <span class="pl-s"><span class="pl-pds">'</span>foo<span class="pl-pds">'</span></span> <span class="pl-k">~</span>/.bash_history<span class="pl-k">;</span> <span class="pl-k">then</span>
  <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span>You appear to have typed 'foo' in the past<span class="pl-pds">"</span></span>
<span class="pl-k">fi</span></pre></div>
